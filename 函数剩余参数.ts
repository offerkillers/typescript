(()=>{
    //函数剩余参数
    //剩余参数是放在函数声明的时候所有的参数的最后
    function showMsg(str:string,...args:string[]){
        console.log(str)  //a
        console.log(args) //剩余参数
    }   
    // showMsg('a','b','c')
})()