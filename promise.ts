(() => {
  enum PromiseStates {
    PENDING = "pending",
    FULFILLED = "fulfilled",
    REJECTED = "rejected",
  }
  class myPrtomise {
    resolveFn: Function = () => {};
    rejectFn: Function = () => {};
    constructor(resolve: any, reject: any) {
      this.resolveFn = resolve;
      this.rejectFn = reject;
    }
    PromiseState: string = PromiseStates.PENDING; //初始化实例状态
    PromiseResult: object = {};

    resolve(): void {
      //1.改变状态
      if (this.PromiseState === PromiseStates.PENDING) {
        this.PromiseState = PromiseStates.FULFILLED;
        this.PromiseResult = this.resolveFn;
        this.resolveFn();
        console.log("resolveFn");
        console.log("this.PromiseState", this.PromiseState);
      }
    }
    reject(): void {
      if (this.PromiseState === PromiseStates.PENDING) {
        this.PromiseState = PromiseStates.REJECTED;
        this.PromiseResult = this.rejectFn;
        this.rejectFn();
        console.log("rejectFn");
        console.log("this.PromiseState", this.PromiseState);
      }
    }
    /**
     * 
     * @param onFulfilled .then成功的回调
     * @param onRejected .then失败的回调
     */
    then(onFulfilled: any, onRejected: any): void {
      if (this.PromiseState === PromiseStates.FULFILLED) {
        onFulfilled(this.PromiseResult);
      } else if (this.PromiseState === PromiseStates.REJECTED) {
        onRejected(this.PromiseResult);
      }
    }
  }
  const myPro = new myPrtomise(
    () => {
      console.log("resove");
    },
    () => {
      console.log("reject");
    }
  );
  myPro.reject();
  console.log("promise");
})();
