const fs = require("fs");
const reject = {};
const stateField = {
  PENDING: "pending",
  FULFILLED: "fulfilled",
  REJECTED: "rejected",
};
class myPromise {
  rejectArgs = {}; //错误参数
  resolveArgs = {};
  state = "";
  result = {};
  cb = [];
  resolveToolsFn = function (err) {
    this.state = stateField.FULFILLED;
    this.rejectArgs = err;
    this.result = err;
  };
  rejectToolsFn = function (data) {
    this.state = stateField.REJECTED;
    this.resolveArgs = data;
    this.result = data;
  };
  reject = function (reject) {
    this.cb.push({ fn: reject });
    console.log(this.cb, "this.cb");
  };
  resolve = function (resolve) {
    this.cb.push({ fn: resolve });
    console.log(this.cb, "this.cb");
  };
  then = function (resolve, reject) {
    //遇到了this问题
    console.log(this,'--')
    if (this.state == stateField.FULFILLED) {
      this.resolve(resolve);
    } else if (this.state == stateField.REJECTED) {
      this.reject(reject);
    }
  };
  constructor(callback) {
    callback(this.resolveToolsFn(this), this.rejectToolsFn(this));
  }
}
const myPro = new myPromise(function (resolve, reject) {
  fs.readFile("./readme.md", function (err, data) {
    if (err) {
      console.log("error");
      reject(err);
    } else {
      console.log("data",data.toString());
      resolve(data);
    }
  });
});
myPro.then(
  function (data) {
    console.log(data, "data");
  },
  function (err) {
    console.log(err, "err");
  }
);
