//抽象类和抽象方法
//抽象类包含抽象方法和实例方法，抽象类不可以被实例化
//作用化-为了让子类进行实例化以及实现内部的抽象方法
//抽象方法不能具体实现
//抽象类中的属性和方法都是为子类服务
(() => {
  abstract class Animal {
      abstract name:string;
    // abstract eat(): void {
    //     console.log('all-animalas-eat')
    // }
    abstract eat()
    run(){
        console.log('非抽象方法，arun')
    }
    sayhi(){
        console.log('hi-evnery')
    }
  }
//   const ani:Animal = new Animal()  //
//   抽象类并不能被直接实例化
class dog extends Animal{
    //重新实现抽象类中的方法
    //此时这个方法是当前dog的实例方法
    name:string = "pet"
    eat() {
        console.log(this.name+'-dog-eat')
    }
}
const erdog = new dog();
// erdog.eat()
})();
