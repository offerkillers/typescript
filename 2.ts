//接口-一种能力一种约束
(function () {
  interface Person {
    name: string;
    age: number;
  }
  // console.log("这里是2.ts");
  function a(person: Person): void {
    // console.log(person.name + " " + person.age, "-n-a-m-e");
  }
  const person = {
    name: "wangwu",
    age: 23,
  };
  //将接口接口作为函数参数
  a(person);
})();
