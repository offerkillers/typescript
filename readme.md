---
## 
配置一个tsconfig.js
当ts文件改变时，就会自动编译ts到一个指定目录
在index.js中引入ts打包后的路径

当ts文件修改时，会自动对其编译，造成打包后的js文件改变，js文件一旦改变，webpack就会立即打包
##
---
npm run dev  //--webpack-dev-serve
npm run build  // --build
