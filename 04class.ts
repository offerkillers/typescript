(()=>{
    // console.log("04-class-ts");
    interface IFLY{
        fly()
    }

    class Person implements IFLY{
        fly() {
            // console.log('super cam flu ,OMG!')
        }
    }

    const one = new Person()
    one.fly()

    interface IJUMP{
        jump()
    }

    class man implements IFLY,IJUMP{
        fly() {
            // console.log('persona')
        }
        jump() {
            // console.log('person-jump-')
        }
    }
    const two = new man()
    two.fly()
    two.jump()


    class Fish{
        name:string
        age:number
        size:string
        type:string
        constructor(name:string='汗汗',age:number,size:string,type:string){
            this.name = name
            this.age = age
            this.size = size
            this.type = type
        }
        say(){
            // console.log(this.name+' '+this.age+' '+ this.size+' '+ this.type+' ')
        }
    }
    const big = new Fish('',2,'big','鲫鱼')
    big.say()
})()