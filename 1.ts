// import './a.js'
(function(){
   class myclass {
    name:string;
    age:number;
   }
   // console.log('这里是1.ts')
   function a(name:string):void{
      // console.log(name,'-n-a-m-e')
   }
   a('zhangsan')


   interface Dog {
      readonly id:number,
      name:string,
      age:number,
      sex?:string
   }

   const kite:Dog = {
      id:1,
      name:'hurry',
      age:3,
      sex:'公'
   }
   // kite.id= 20;
   console.log(kite.name,'Dog-kite-object-name')

   interface fnsign{
      //签名
      //定义一个调用签名
      (source:string,subString:string):boolean
   }
   const fn:fnsign = function(source:string,subString:string):boolean{
      return source.search(subString)> -1
   }
})()