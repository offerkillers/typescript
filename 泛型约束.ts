(() => {
  //但一个为泛型的属性取用length值时会报错。--泛型约束
  // function getlength<T>(x:T):number{
  //     return x.length
  // }
  //错误实例，类型T上不存在属性length
  interface mylength {
    //接口中有一个属性Length
    length: number;
  }
  function getlength<T extends mylength>(x: T): number {
    return x.length;
  }
  console.log(getlength<mylength>('111-length'))
})();
