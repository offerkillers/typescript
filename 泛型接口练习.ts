(() => {
  class Dog {
    theme: string;
    name: string;
    height: string;
    id: number;
    constructor(theme: string, name: string, height: number, id?: number) {
      this.theme = theme;
      this.name = name;
      this.height = height + "cm";
      this.id = id;
    }
  }
  interface Gloden<T> {
    data: Array<T>;
    add:(t: T) => T;
    introduct: (t: T) => string;
    canleder: (t: T) => string;
  }
  class GlodenDog implements Gloden<Dog> {
    data: Array<Dog> = [];
    add(dog:Dog):Dog{
      dog.id = new Date().getTime();
      this.data.push(dog)
      return dog
    }
    introduct(dog: Dog): string {
        console.log(dog.theme+' '+ dog.name+' '+dog.height+' '+dog.id)
        return '';
    }
    canleder(dog: Dog){
        return dog.height
    };
  }
  const yellowhair = new GlodenDog()
  //'yellow','小黄',100,'1'
  yellowhair.add(new Dog('yellow','小黄',100)) 
  // console.log(yellowhair.data)
  yellowhair.introduct(new Dog('green','小绿',80))
})();
