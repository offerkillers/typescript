(() => {
  //存取器-有效的控制对对象中成员的访问
  //外部可以传入姓氏和名字，同时使用set,get控制姓名的属性，外部也可以进行修改
  class Person {
    private _name: string;
    _lastName: string;
    constructor(name: string, lastName: string) {
      this._name = name;
      this._lastName = lastName;
    }
    public get name() {
      return this._name;
    }
    public set name(val){
        this._name = val
     }
  }
  const peter = new Person("东方", "不败");
//   console.log(peter, "111");
//   console.log(peter.name, "peter-name");
  peter.name = "西方";
//   console.log(peter.name, "peter-name");
})();
