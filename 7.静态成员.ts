//通过static修饰的属性或者方法，那么就是静态的属性及静态的方法，也称作未静态成员
//静态成员在使用的时候是使用类目的这种语法来调用的

//static namea 爆红
//类中默认有一个内置的namea属性
(()=>{
    class Person7 {
        static namea:string;
        constructor(namea:string){
            //当namea被static修饰之后。namea是静态属性，不能通过实例对象直接调用静态属性来使用
            //通过类名.静态属性的方式来访问该成员变量
            //通过类目.静态属性的方式来修改成员变量
            //通过类名.静态方法的方式调用内部的属性和方法
            //构造函数不能使用static-- 构造函数不能使用static来修饰
            //优点：可以不实例化对象来调用类的属性和方法


            // this.namea = namea;
        }
        sayhi(){
            // console.log(this.namea,'-t-h-i-s-namea---:')
        }
    }
    const sweet = new Person7('霄汉')
    // console.log(Person7.namea,'n-amea')   //underfinded
    sweet.sayhi()
})()