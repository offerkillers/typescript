(function () {
  // console.log("class-ts");
  interface Person {
    firstName: string;
    lastName: string;
  }
  //定义一个类
  class PersonOne {
    //定义公共的字段
    public firstname: string;
    lastname: string;
    fullname: string;
    constructor(firstname: string, lastname: string) {
      this.firstname = firstname;
      this.lastname = lastname;
      this.fullname = firstname + lastname;
    }
  }

  //实例化对象
  const person = new PersonOne("诸葛", "孔明");
  
  const person1 = new PersonOne("诸葛", "孔明");
})();
