(()=>{
    //泛型类
    //定义一个类，类中的属性值得类型是不确定的，方法中的参数及返回值的类型是不确定
    class Genera<T>{
        value:T
        add:(x:T,y:T) => T
    }
    const a:Genera<string> = new Genera<string>()
    a.value = '泛型类'
    a.add = function(x,y){
        return x+' '+y
    }
    // console.log(a.add('ele','fn')+' '+ a.value)
})()