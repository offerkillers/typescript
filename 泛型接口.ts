(() => {
  //需求：定义一个类，用来存储用户的相关信息-（id,名字，年龄
  //需求-通过一个类的实例对象调用add方法可以添加多个用户信息对象。
  //---调用getUserId方法可以根据id获取某个指定的用户信息对象
  //抽象类
  class User {
    id?: number;
    name: string;
    age: number;
    constructor(name: string, age: number, id?: number) {
      this.name = name;
      this.age = age;
      this.id = id;
    }
  }
  //定义一个泛型接口
  interface IBaseCRUD<T> {
    data: Array<T>;
    add: (t: T) => T;
    getUserId: (id: number) => T;
  }
  class UserCRUD implements IBaseCRUD<User> {
    //用来保存多个User类型的用户信息对象
    data: Array<User> = [];
    add(user: User): User {
      //产生id
      user.id = new Date().getTime();
      //添加用户信息对象添加到data数组中
      this.data.push(user);
      return user;
    } //存储用户信息对象的
    getUserId(id: number): User {
      return this.data.find((user) => {
        user.id == id;
      });
      //   return new User();
    } //根据id查询指定的用户信息
  }

  //实例化添加用户信息对象的类
  const usercrud: UserCRUD = new UserCRUD();
  usercrud.add(new User("张三", 23));
  // console.log(usercrud.getUserId(23));
  // console.log(usercrud)
})();
