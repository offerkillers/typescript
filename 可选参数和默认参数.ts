// import { stringify } from "querystring"

(()=>{
    //定义一个函数：传入姓氏和名字，可以得到姓名
    //需求:如果不传入任何的内容，就返回默认的姓氏
    //如果只参入姓氏，只返回姓氏
    function getFullName(firstName?:string,lastName?:string):string{
        if(!firstName){
            //返回默认值
            return '默认值';
        }
        // debugger
        if(!lastName){
            return firstName
        }
        return firstName+lastName;
    }
    // console.log(getFullName('zhang'))
    // console.log(getFullName('zhang','san'))
    // console.log(getFullName())
})()