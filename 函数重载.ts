(()=>{
    //ts的函数重载不同于其他语言
    //函数重载声明
    function add(x:number,y:number):number
    function add(x:string,y:string):string
    //未被声明的则会报错

    function add(x: number|string, y: number|string): number|string {
        if(typeof(x)=='number'&&typeof(y)=='number')
        {return x + y;}
        else if(typeof(x)=='string'&&typeof(y)=='string'){
            return +x+y;
        }
      }
    //   console.log(add(3, 4), "add-result");
    //   //7
    //   console.log(add('诸葛', '亮'), "add-result");
      //34
    //   console.log(add('3', 4), "add-result");
      //underfinded
})()