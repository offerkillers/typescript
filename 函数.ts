(() => {
  //函数
  //封装一些重复使用的代码，在需要时候直接调用既可
  //函数声明-表达式-返回值
  //ts中的书写可以定义参数类型
  //ts不支持函数重载

  function add(x: number, y: number): number {
    return x + y;
  }
  // function add(x:number):number{}
  // console.log(add(3, 4), "add-result");
  //函数的完整写法
  const addone: (x: number, y: number) => number = function (
    x: number,
    y: number
  ): number {
    return x + y;
  };
})();
